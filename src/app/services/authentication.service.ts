import {Injectable} from '@angular/core';
import {AngularFireAuth} from "@angular/fire/compat/auth";
import {catchError, from, map, Observable, throwError} from "rxjs";
import {error} from "@angular/compiler-cli/src/transformers/util";

@Injectable({
  providedIn: 'root'
})
export class AuthenticationService {

  userEmail: string = "";

  constructor(
    private auth: AngularFireAuth
  ) {
  }

  signIn(params: SignIn): Observable<any> {
    localStorage.setItem("userEmail", params.email)
    return from(this.auth.signInWithEmailAndPassword(
      params.email, params.password
    )).pipe(
      catchError((error: FirebaseError) =>
        throwError(() => new Error(this.translateFirebaseErrorMessage(error)))
      )
    );
  }

  getUserEmail(): string | null {
    return localStorage.getItem("userEmail") ? localStorage.getItem("userEmail") : "";
  }

  signUp(params: SignUp): Observable<any> {
    return from(this.auth.createUserWithEmailAndPassword(
      params.email, params.password
    )).pipe(
      catchError((error: FirebaseError) =>
        throwError(() => new Error(this.translateFirebaseErrorMessage(error)))
      )
    );
  }

  isAuthenticatedUser(): Observable<boolean> {
    return this.auth.authState.pipe(map(user => !!user));
  }

  logout(): Observable<void> {
    localStorage.removeItem("userEmail");
    return from(this.auth.signOut()).pipe(
      catchError((error: FirebaseError) =>
        throwError(() => new Error(this.translateFirebaseErrorMessage(error)))
      )
    );
  }

  private translateFirebaseErrorMessage({code, message}: FirebaseError) {
    if (code === "auth/user-not-found") {
      return "User not found.";
    }
    if (code === "auth/wrong-password") {
      return "User not found.";
    }
    return message;
  }

}

type SignIn = {
  email: string;
  password: string;
}

type SignUp = {
  email: string;
  password: string;
}

type FirebaseError = {
  code: string;
  message: string
};
