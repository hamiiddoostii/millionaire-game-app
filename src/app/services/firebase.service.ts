import {Injectable} from '@angular/core';
import {AngularFirestore} from "@angular/fire/compat/firestore";
import {Observable, of} from "rxjs";

@Injectable({
  providedIn: 'root'
})
export class FirebaseService {

  constructor(private angularFirestore: AngularFirestore) {
  }

  getQuestionList(): Observable<any[]> {
    return this.angularFirestore.collection("questions").snapshotChanges();
  }

  addScoreToUserHistory(userEmail: string | null, score: number): Promise<void> {
    const userScoreHistoryRef = this.angularFirestore.collection(`users/${userEmail}/historyScore`);
    const docId = this.angularFirestore.createId();
    let currentTime: string = new Date().toLocaleString();
    const scoreData = {
      currentTime,
      score
    };

    return userScoreHistoryRef.doc(docId).set(scoreData);
  }

  getUserScoreHistory(userEmail: string | null): Observable<any[]> {
    const userScoreHistoryRef = this.angularFirestore.collection(`users/${userEmail}/historyScore`);
    return userScoreHistoryRef.valueChanges();
  }
}
