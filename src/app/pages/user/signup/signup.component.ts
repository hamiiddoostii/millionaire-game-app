import {Component, OnInit} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from "@angular/forms";
import {AuthenticationService} from "../../../services/authentication.service";
import {Router} from "@angular/router";
import {MatSnackBar} from "@angular/material/snack-bar";

@Component({
  selector: 'app-signup',
  templateUrl: './signup.component.html',
  styleUrls: ['./signup.component.scss']
})
export class SignupComponent implements OnInit {

  form!: FormGroup;
  isRegister: boolean = false;

  constructor(
    private authenticationService: AuthenticationService,
    private formBuilder: FormBuilder,
    private router: Router,
    private snackBar: MatSnackBar
  ) {
  }

  ngOnInit(): void {
    this.form = this.formBuilder.group({
      email: ['', [Validators.required, Validators.email]],
      password: ['', [Validators.required, Validators.minLength(6)]],
      confirmPassword: ['', [Validators.required, Validators.minLength(6)]]
    });
  }

  register() {
    this.isRegister = true;
    if (this.form.valid) {
      if (this.form.value.password != this.form.value.confirmPassword) {
        this.isRegister = false;
        this.snackBar.open("Passwords do not match", "OK", {
          duration: 5000
        })
      } else {
        this.authenticationService.signUp({
          email: this.form.value.email,
          password: this.form.value.password
        }).subscribe({
          next: () => this.router.navigate(['auth']),
          error: error => {
            this.isRegister = false;
            this.snackBar.open(error.message, "OK", {
              duration: 5000
            })
          }
        });
      }
    }
  }

}
