import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { HomeRoutingModule } from './home-routing.module';
import { HomeComponent } from './home.component';
import { GameComponent } from './game/game.component';
import {MatListModule} from "@angular/material/list";
import {MatCardModule} from "@angular/material/card";
import {MatButtonModule} from "@angular/material/button";
import {MatRadioModule} from "@angular/material/radio";
import {FormsModule, ReactiveFormsModule} from "@angular/forms";
import {MatStepperModule} from "@angular/material/stepper";
import { MatProgressSpinnerModule } from '@angular/material/progress-spinner';
import {ScoreHistoryComponent} from "./score-history/score-history.component";
import {MatTableModule} from "@angular/material/table";


@NgModule({
  declarations: [
    HomeComponent,
    GameComponent,
    ScoreHistoryComponent
  ],
  imports: [
    CommonModule,
    HomeRoutingModule,
    MatListModule,
    MatCardModule,
    MatButtonModule,
    MatRadioModule,
    MatProgressSpinnerModule,
    FormsModule,
    MatStepperModule,
    ReactiveFormsModule,
    MatTableModule
  ]
})
export class HomeModule { }
