import {Component, Input} from '@angular/core';

@Component({
  selector: 'app-score-history',
  templateUrl: './score-history.component.html',
  styleUrls: ['./score-history.component.scss']
})
export class ScoreHistoryComponent {

  @Input() scoreHistory: any[] = [];
  displayedColumns: string[] = ['dateAndTime', 'scoreHistory'];
}
