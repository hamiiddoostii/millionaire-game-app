import {Component, OnInit, ViewChild} from '@angular/core';
import {FirebaseService} from "../../../services/firebase.service";
import {Router} from "@angular/router";
import {FormBuilder, FormGroup, Validators} from "@angular/forms";
import {MatSnackBar} from "@angular/material/snack-bar";
import {AuthenticationService} from "../../../services/authentication.service";
import {Observable} from "rxjs";

@Component({
  selector: 'app-game',
  templateUrl: './game.component.html',
  styleUrls: ['./game.component.scss']
})
export class GameComponent implements OnInit {

  questions: question[] = [];
  selectedQuestions: question[] = [];
  userResponses: {
    [questionId: string]: {
      answer: string,
      status: boolean
    }
  } = {};
  isLinear = true;
  totalCorrectPoints: number = 0;
  isLoading: boolean = true;
  userScore: number = 0;
  userEmail: string | null = "";
  scoreHistory: any[] = [];
  questionForm: FormGroup;
  @ViewChild('stepper') stepper: any;

  constructor(
    private firebaseService: FirebaseService,
    private authService: AuthenticationService,
    private fb: FormBuilder,
    private _snackBar: MatSnackBar
  ) {
    this.questionForm = this.fb.group({
      selectedAnswer: ['', Validators.required],
    });
  }

  ngOnInit() {
    this.userEmail = this.authService.getUserEmail();

    this.firebaseService.getUserScoreHistory(this.userEmail).subscribe(score => {
      console.log(score);
      if (score) {
        this.scoreHistory = score;
      }
    });
    this.firebaseService.getQuestionList().subscribe(res => {
      this.questions = res.map(e => {
        return {
          id: e.payload.doc.id,
          ...e.payload.doc.data() as {}
        } as question;
      });
      this.selectRandomQuestions(5);
      this.isLoading = false;
    });
  }

  selectRandomQuestions(count: number) {
    const shuffledQuestions = [...this.questions];
    for (let i = shuffledQuestions.length - 1; i > 0; i--) {
      const j = Math.floor(Math.random() * (i + 1));
      [shuffledQuestions[i], shuffledQuestions[j]] = [shuffledQuestions[j], shuffledQuestions[i]];
    }
    this.selectedQuestions = shuffledQuestions.slice(0, count);
  }

  submitAnswer(questionIndex: number) {
    const question = this.selectedQuestions[questionIndex];
    const selectedAnswer = this.questionForm.get('selectedAnswer')?.value;

    const points = this.calculateQuestionPoints(question, selectedAnswer);
    this.totalCorrectPoints += points;
    const isCorrect = points > 0;

    this.userResponses[question.id] = {
      answer: selectedAnswer,
      status: isCorrect,
    };

    let message: string;
    let panelClass: string;

    if (isCorrect) {
      message = 'Correct answer!';
      panelClass = 'correct-snackbar';
    } else {
      message = `Incorrect answer. Correct answer(s): ${question.correctAnswers + ', '}`;
      panelClass = 'incorrect-snackbar';
    }

    this._snackBar.open(message, 'Close', {
      duration: 10000,
      verticalPosition: 'top',
      panelClass: [panelClass]
    });

    this.stepper.next();
    this.questionForm.get('selectedAnswer')?.setValue('');
  }

  onStepSelectionChange(event: any) {
    if (event.selectedIndex === this.selectedQuestions.length) {
      this.updateUserScore();
    }
  }

  updateUserScore() {
    this.firebaseService.addScoreToUserHistory(this.userEmail, this.totalCorrectPoints)
      .then(() => {
        this.userScore = this.totalCorrectPoints;
      })
      .catch(error => {
        console.error('Failed to add user with score:', error);
      });
  }

  calculateQuestionPoints(question: question, selectedAnswer: string): number {
    const correctAnswers = question.correctAnswers;
    const isCorrect = correctAnswers.includes(selectedAnswer);

    if (isCorrect) {
      // Points divided equally among correct answers
      return question.points / correctAnswers.length;
    } else {
      // Deduct points for incorrect answers
      return -question.points / correctAnswers.length;
    }
  }

  calculateTotalPoints(): number {
    return this.selectedQuestions.reduce((total, question) => total + Number(question.points), 0);
  }

}

type question = {
  id: string;
  question: string;
  choices: string;
  correctAnswers: string;
  points: number;
}
