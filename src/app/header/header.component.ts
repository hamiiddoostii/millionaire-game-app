import {Component, OnInit} from '@angular/core';
import {AuthenticationService} from '../services/authentication.service';
import {Router} from '@angular/router';
import {map} from 'rxjs';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent {

  constructor(private authService: AuthenticationService,
              private router: Router) {
  }

  logout(): void {
    this.authService.logout().subscribe(() => {
      this.router.navigate(['/auth'])
    });
  }
}
